export const images = {
  seeProblems: 'https://us-east-1.tchyn.io/snopes-production/uploads/2016/07/math.jpg?resize=865%2C452',
  // createExam: 'http://blogs.staffs.ac.uk/student-blogs/files/2016/01/exam-week-pencil-and-paper.jpg',
  createExam: 'https://c.ndtvimg.com/exam_625x300_1530267263191.jpg',
  submitProblem: 'https://image.freepik.com/fotos-gratis/blackboard-com-uma-soma-de-um-ponto-de-interrogacao-e-uma-lampada_1205-371.jpg',
  profileButton: 'https://www.weact.org/wp-content/uploads/2016/10/Blank-profile.png',
  latex: 'https://pangea.stanford.edu/computing/unix/formatting/latexexample8.jpg',
  jeremy: 'https://www.dcc.uchile.cl/sites/default/files/fotografias/profesores/barbay.jpg',
  bird: 'profileImg.png',
  dario: 'dario.jpg',
  robinson: 'robinson.jpg',
  roberto: 'roberto.jpg',
  pablo: 'pablo.jpg',
};