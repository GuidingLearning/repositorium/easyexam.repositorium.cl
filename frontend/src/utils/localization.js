import { spanish } from "../locales/spanish";
import { english } from "../locales/english";

export const locales = {
  spanish,
  english,
};